const HTML5ToPDF = require("html5-to-pdf")
async function run () {
  const pdf = new HTML5ToPDF({
    templatePath:'./',
    inputPath:'./index.html',
    outputPath:'./cv.pdf',
    pdf:{
      landscape: true,
      printBackground: true,
      format: 'A4',
      margin: {top:0,left:0,right:0,bottom:0},
      pageRanges: '1',
      scale:0.99
    },
    launchOptions:{
      args:[
        "--no-sandbox",
        "--disable-setuid-sandbox"
      ]
    }
  });
  console.log(pdf)
  await pdf.start()
  await pdf.build()
  await pdf.close()
}

run()
